"use strict";

const fs = require("fs");

var createZonPackageString = function (cellCount, zones) {
    let zbudZoneString = "# ZON: Zone input file created on " + new Date() + "\n";

    zbudZoneString += cellCount + "\n";
    zbudZoneString += "internal 1 (free) -1 zone numbers\n";

    let increment = 10;
    for (let i = 1; i <= cellCount; i++) {
        let defaultZone = true;

        for (let j = 0; j < zones.length; j++) {
            let zone = zones[j].zoneNumber;
            let zoneCells = zones[j].groupCells;

            if (zone === 1) {
                continue;
            }

            if (zoneCells.includes(i)) {
                defaultZone = false;

                if (i % increment === 0) {
                    zbudZoneString += zone + "\n";
                } else {
                    zbudZoneString += zone + "\t";
                }

                break;
            }
        }

        if (defaultZone) {
            if (i % increment === 0) {
                zbudZoneString += 1 + "\n";
            } else {
                zbudZoneString += 1 + "\t";
            }
        }
    }

    return zbudZoneString;
};

var translate = function (inputData, zoneInputFile) {
     try {
         console.log("Translating input zone file...");

         let cellCount = inputData.cellCount;
         let zones = inputData.zones;
         let content = createZonPackageString(cellCount, zones);
         fs.writeFileSync(zoneInputFile, content, "utf8");

         console.log("Input zone file translation completed!");
     } catch (err) {
         console.log(err);
     }
};

module.exports = {
    translate: translate
};
