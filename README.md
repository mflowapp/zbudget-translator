# Zone Budget Zone Input File Translator #

Zone budget zone input file translator is a command line Node.js application that translates cell level zone budget data to a zone input file used by ZONBUDUSG program. It is built on Node.js v6.9.3.

### Command Line Format ###

* node index <file path of payload.json> <file path of *.zon>

### Content of payload.json file ###

```
#!javascript

{
    "cellCount": 9,
    "zones":[
        {
            "zoneNumber": 2,
            "groupCells":[2, 5, 8]
        }
    ]

}
```
* Note: zone 1 is the default zone, it does not need to appear in the input json

### Task manager call ###

```
#!javascript
{
    "taskid":"taskid",
        "input": [
        {
            "name": "zbudinput",
            "format": "json",
            "dataPath": "/storage/zbudinput.json"
        }
    ],
    "outputConfig": [
        {
            "name": "zbudoutput",
            "format": "zon",
            "dataPath": "/storage/zbudresult.zon"
        }
    ]
}

```