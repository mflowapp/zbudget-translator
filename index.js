#!/usr/bin/env node

"use strict";

var fs = require("fs");
var program = require("commander");
var zbudgetTranslator = require("./zbudgettranslator");

console.log("Zone budget zone input file translator");

program
    .arguments("<intputFile> <outputFile>")
    .action(function (inputFile, outputFile) {
        console.log("Input file: %s\nOutput file: %s\n", inputFile, outputFile);
        try {
            var payloadString = fs.readFileSync(inputFile, "utf8");
            var inputData = JSON.parse(payloadString);
            zbudgetTranslator.translate(inputData, outputFile);
        } catch (err) {
            console.log(err);
        }
    })
    .parse(process.argv);
